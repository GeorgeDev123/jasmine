this.property = {
    streetNo: 1,
};
this.property1 = {
    streetNo: 2
};
this.matter = {
    propertyList: [this.property, this.property1]
};

this.volume3 = "Volume3";
this.folio = "folio";
exports.variables = function(){
    return {
        matter: matter
    }
}
exports.properties = function(){
    var result="";
    if(matter.propertyList[1].streetNo){
        result = "properties";
    }
    else{
        result = "a property";
    }
    return result;
}
exports.actVolume = function(){
    var result="";
    result += this.volume3 + ":" + this.folio;
    return result;
}