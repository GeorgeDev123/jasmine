var calculator = require("../calculator");
describe("property", function(){

    var volume3 = "Volume3";
    var folio = "folio";
    beforeEach(function () {
        this.volume3 = "Volume3";
        this.folio = "folio";
    });
    it("should get properties", function(){
        var result = calculator.properties();
        expect(result).toBe("properties");
    });
    it("should get Volume3:folio", function(){
        var result = calculator.actVolume(volume3,folio);
        expect(result).toBe("Volume3:folio");
    })
});